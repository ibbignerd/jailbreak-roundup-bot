# README #

This reddit program gathers reddit posts based on post titles and comments, sends them daily to those subscribed and generates a RoundUp post to be submitted weekly.

The program should run once through and stop as it will be set to be executed on a timed schedule.

**Please pull and merge before submitting a pull request**

### Outline of program functions ###

1. Search Inbox
    1. Get unread messages
    2. If PM
        1. if admin, check for commands
        2. if not admin
            1. if subscription command request
            2. if unrecognized, forward to admin
    3. If comment/post reply
        1. Forward to admin
2. Scan titles
    1. loop through 100 most recent submissions
        1. If title contains keyword and not in database
            1. Process and submit to database with necessary information
            2. Commit to database with SQL
3. Scan comments
    1. loop through 500 most recent comments
        1. if comment contains keyword and not in database
            1. Process and submit to database with necessary information
            2. Commit to database with SQL
            3. Reply to comment with appropriate response
4. Generate RoundUp
    1. Access database and generate RoundUp thread. 
    2. Output should be in [this format](https://www.reddit.com/r/jailbreak/comments/2wwkuw/roundup_rjailbreak_february_16th_22nd/)
5. Clean up database
    1. To be called after Generate RoundUp
    2. Records older than two weeks should be removed from database