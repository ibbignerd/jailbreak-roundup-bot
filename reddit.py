# Code created by /u/ibbignerd and contributers.
# Please read repo readme for information before coding
# LICENSE: Please link back to this repo. 

import praw
import datetime
import logging
import sqlite3
import time
import traceback
import random
from dateutil import parser

# Bot reddit username
sUsername = ''
# Bot reddit password
sPassword = ''
# Build version
sVersion = ''
# reddit user agent
sUseragent = ''
# Subreddits to search (array)
sSubreddit = []
# reddit username of the admin of the bot
sAdmin = ''
# Search for these strings
sTitlestring = ["[release", "[update", "[upcoming", "[announcement", "[beta", "[meta", "[finish", "[tutorial", "[preview", "[wip", "[guide"]
# Check for the following username mentions
cBotmention = ["/u/roundup_bot", "/u/roundupbot"]
# Comment footer that goes on all comments
cFooter = '\n\n------\n\n^[[Info]](http://www.reddit.com/r/ibbignerd_/wiki/roundup_bot) ^[[Subscribe]](http://www.reddit.com/message/compose/?to=RoundUp_bot&subject=subscribe&message=add) ^[[Source]](https://bitbucket.org/ibbignerd/roundup_bot/overview) ^[[Mistake?](http://www.reddit.com/message/compose/?to=ibbignerd&amp;subject=%2Fu%2FRoundUp_bot%20messed%20up;message=%0A%0A%0A%0APlease%20leave%20the%20link%20below%20unaltered%0A%0A'
# Ignore users that spam
sIgnoreuser = [""]

# Comment replies
# 0: Normal
# 1: Self aware
# 2: Request post
# 3: Already added
# 4: Wrong name
# 5: Language
cCommentreply =  ["Thank you for contacting me. I will tell /u/ibbignerd you think this post should be in the next /r/Jailbreak RoundUp thread.", "I'm not trying to be completly self aware, but why are you trying to add the thread I created to myself. We wouldn't want a paradox to kill me, right? \n\n ^Actually, ^take ^this ^just ^to ^be ^safe, ^\"This ^statement ^is ^false\".", "The idea of the RoundUp is to see what tweaks are going to come out. A request thread holds no such promise. Because of this, it won't be added to the RoundUp. Thank you, though!", "I have already added this thread to the list. Thank you though!", "I don't know of whom you speak. My name is /u/RoundUp_bot.", "Please do not use such language. It hurts my feelings when you say things like this..."]
cBadwords = ["fuck", "bitch", "pussy", "shit", "fucking", "fucker"]

# Comment replies to admin
cObedient = ["Right away, sir!", "Already done!", "I'll have that done right after I finish my coffee.", "Now that that's done, I'm going to take a break", "How much longer do I have to do this job?", "Done!", "As you wish, master.", "Keep being awesome, sir.", "Keep up the good work", "Keep being awesome.", "Can we talk about that raise now?", "After this, can I please have some water?", "Psst, you should upgrade my response system." ]


try:
    import bot
    # This is a file in my python library which contains my
    # Bot's username and password.
    # I can push code without showing credentials
    sUsername = bot.uG
    sPassword = bot.pG
    sUseragent = bot.aG
    sVersion = bot.vG
    sSubreddit = bot.sG
    sAdmin = bot.dG
except ImportError:
    pass


# INFO: Returns: [08-05 12:07:53] [LEVEL ] message in console
FORMAT = '[%(asctime)-13s] [%(levelname)-6s] %(message)s'
DATE_FORMAT = '%m-%d %H:%M:%S'
formatter = logging.Formatter(fmt=FORMAT, datefmt=DATE_FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
log = logging.getLogger(__name__)
# INFO: Set logging level: INFO, DEBUG, WARNING, ERROR
log.setLevel(logging.DEBUG)
log.addHandler(handler)

# INFO: Connect to sql.db file (create if doesn't exist)
sql = sqlite3.connect('sql.db')
log.info('Loaded SQL Database')
# INFO: Create cursor to carry out SQL arguments
cur = sql.cursor()

# INFO: Create
cur.execute('CREATE TABLE IF NOT EXISTS saveposts(ID TEXT, SUB TEXT, TYPE TEXT, TITLE TEXT, UTC INTEGER, AUTHOR TEXT)')
# Thing ID | Subreddit | Type | Title | UTC_CREATED (h-5=CST) | Author
cur.execute('CREATE TABLE IF NOT EXISTS subscriptions(ID TEXT, SUB TEXT)')
sql.commit()
log.info('Loaded Completed table')



# Log into reddit account
r = praw.Reddit(sUseragent + sVersion)
r.login(sUsername, sPassword)
log.info('Logged into reddit')

# reddit limitations
# 100 comments every minute
# 20 posts every minute
# More API info found at reddit.com/api

# Subscribe and unsubscribe from subscription database. Loaded from 'subscriptions' sql table
def scanInbox():
    pms = r.get_unread(update_user=True, limit=100)
    for pm in pms:
        # Check if account has been deleted
        try:
            author = pm.author.name
        except:
            log.warning("NoneType returned in message")
            pm.mark_as_read
            continue
        pbody = pm.body
        # Check if message is not a comment reply
        if not pm.was_comment:
            if author == sAdmin:
                # Used for the admin to send commands to bot to exacute via private message
                log.warning(author + ' sent the command: ' + pbody)
                pbody_low = pbody.lower()
                if pbody_low == "help":
                    response = ("##Help Output\n\n"
                                "1. isAlive - Returns \"I am alive\"\n"
                                "2. generate \n"
                                "    1. [subreddit] - Return all threads in 'saveposts'\n"
                                "3. remove [submission id] - Remove thread from 'saveposts'")
                    pm.reply(response)
                elif pbody_low == "isalive":
                    pm.reply("I am alive")
                elif pbody_low.startswith("generate"): #generate [subreddit]
                    log.debug("Starting 'generate' command for " + author)

                    response = generateRoundUp(pbody_low, "week")

                    r.submit('ibbignerd_', response[1], text = response[0])
                elif pbody_low.startswith("remove"):
                    look = pbody_low.split(" ")
                    params = "UPDATE saveposts SUB ='', TYPE ='', TITLE ='', UTC ='', AUTHOR ='' WHERE ID = " + look[1]
                    cur.execute(params)
            else:
                # Normal user sending PM
                # subject = subscribe
                # body = add
                if "subscribe" in pm.subject.lower() or "subscription" in pm.subject.lower():
                    if "add" in pbody.lower():
                        if any(sub in pbody.lower() for sub in sSubreddit):
                            cur.execute('INSERT INTO subscriptions VALUES(?,?)', [author, sub])
                            pm.reply("You have been added to the /r/" + sub + " mailing list.\n\n[**Unsubscribe**](http://www.reddit.com/message/compose/?to=RoundUp_bot&subject=subscribe&message=remove)")
                            log.info("Adding " + author + " to the subscriptions database")
                        else:
                            pm.reply("I wasn't able to process this request. Please click one of the links below to subscribe!\n\n/r/jailbreak | [[**Subscribe**](http://www.reddit.com/message/compose/?to=RoundUp_bot&subject=subscribe&message=add%20jailbreak)]\n---|---\n**/r/iOSthemes** | **[[**Subscribe**](http://www.reddit.com/message/compose/?to=RoundUp_bot&subject=subscribe&message=add%20iosthemes)]**")
                    elif "remove" in pbody.lower():
                        log.info("Removing " + author + " from the subscriptions database")
                        cur.execute('DELETE FROM subscriptions WHERE ID = \'' + author + '\'')
                        pm.reply("You have been removed to the mailing list.\n\nTo add yourself back again, just send me another message with the subject of \"subscribe\" and the body as \"add\"")
                else:
                    # Not a subscription command, forward it to admin
                    r.send_message(sAdmin, 'Forwarded private message from /u/' + author, pbody)
                    pm.reply("Hey. You just sent me a message, but I'm just a bot. I forwarded your message on to /u/" + sAdmin + " already. You should contact him if you have any questions or whatnot. \n\n[**Subscribe**](http://www.reddit.com/message/compose/?to=RoundUp_bot&subject=subscribe&message=add) | [**Unsubscribe**](http://www.reddit.com/message/compose/?to=RoundUp_bot&subject=subscribe&message=remove)\n\nHave a great day!")
                    log.warning('Forwarded private message to ' + sAdmin)

        else:
            # Comment reply to post

            #Not sure if I want to use this yet
            #if pm.subject == "post reply":
            #    r.send_message(sAdmin, 'Forwarded post reply by /u/' + author, '[' + pm.link_title + '](' + pm.context +')\n\n' + pbody)
            #    log.warning('Forwarded post reply to ' + sAdmin)
            #Comment reply to comment
            if pm.subject == "comment reply":
                r.send_message(sAdmin, 'Forwarded comment reply by /u/' + author, '[' + pm.link_title + '](' + pm.context +')\n\n' + pbody)
                log.warning('Forwarded comment reply to ' + sAdmin)
        pm.mark_as_read()
    log.info('Done with Inbox')


#              0         1          2         3            4           5
# saveposts(ID TEXT, SUB TEXT, TYPE TEXT, TITLE TEXT, UTC INTEGER, AUTHOR TEXT)
#          Thing ID | Subreddit | Type | Title | UTC_CREATED (h-5=CST) | Author

# Generate the RoundUp text. Returns array with [contents, title]
def generateRoundUp(pbody, genType):
    sec = 86400 #time in a day
    if genType == "week":
        sec = 604800 # Time in a week
    pbody_array = pbody.split(" ") #From the command `generate jailbreak`
    sub = pbody_array[1]
    curTime = time.time() # Time in UTC from epoch
    targetTime = curTime - sec
    targetTime =- 18000 # CST Conversion
    oldDate = "0"
    sqlCounter = 0
    output = ""
    first = True
    titleDate1 = ""
    titleDate2 = ""

    params = (sub, str(targetTime))
    for line in cur.execute("SELECT * FROM saveposts WHERE SUB = ? AND UTC >= ?", params): #Each entry in RoundUp
        date = float(line[4]) + 18000
        date1 = time.strftime('%A, %B ', time.localtime(date)) # Monday, January
        print "date1 = " + date1
        date2 = time.strftime('%d', time.localtime(date)) # 01
        print int(date2)

        # date headers
        if oldDate != date2:
            oldDate = date2
            if str(date2)[-1:] == 1 and date2 != 11:
                date2 += "st"
            elif str(date2)[-1:] == 2 and date2 != 12:
                date2 += "nd"
            elif str(date2)[-1:] == 3 and date2 != 13:
                date2 += "rd"
            else:
                date2 += "th"
            dateFinal = date1 + str(date2).lstrip("0") # Monday, January 1st
            output += "\n\n---\n\n##" + dateFinal + "\n\n"
            sqlCounter = 1
        else:
            sqlCounter += 1

        if first:
            titleDate1 = time.strftime('%B ', time.localtime(date)) + str(date2).lstrip("0")
            first = False
        else:
            titleDate2 = time.strftime('%B ', time.localtime(date)) + str(date2).lstrip("0")


        title = line[3].strip(" ")

        #Torqueo: Rotation Enabled on smaller devices (BigBoss - Free)
        if line[5] == "Ziph0n" or line[5] == "RoundUp_bot":
            try:
                title_ar = title.split(": ")
                # Torqueo: Rotation Enabled on smaller devices (BigBoss - Free) -> ["Torqueo", "Rotation Enabled on smaller devices (BigBoss - Free)"]

                top_ar = title_ar[1].split(" (")
                # Rotation Enabled on smaller devices (BigBoss - Free) -> ["Rotation Enabled on smaller devices", "BigBoss - Free)"]

                title_ar[1] = title_ar[1].replace(" (" + top_ar[1], "")
                # -> ["Torqueo", "Rotation Enabled on smaller devices"]

                info_ar = top_ar[1].replace(")", "")
                # BigBoss - Free) -> BigBoss - Free

                title_info = info_ar.split(" - ")
                # BigBoss - Free -> ["BigBoss", "Free"]

                if title_info[0].lower == "free":
                    title_info[0].capitalize()
                elif "$" not in title_info[0]:
                    title_info[0] = "$" + title_info[0]
                title_info[0].replace(",", ".")


                info = [title_ar[0], title_ar[1], title_info[0], title_info[1]]
                # Tweak name, Description, Repo, Price

                title = info[0] + "**" + "(http://www.reddit.com/r/" + sub + "/comments/" + line[0] + ") - (" + info[3] + ") - " + info[1] + " - " + info[2]
                end = "\n"
            except Exception:
                print(traceback.format_exc())
                continue
        else:
            end = "**](http://www.reddit.com/r/" + sub + "/comments/" + line[0] + ")\n"
            # TODO: Get description, price, and repo from cydiaupdates
        output += str(sqlCounter) + ". [" + line[2] + " **" + title + end
        # /---- End For Loop ----/
    if genType == "week":
        postTitle = "[RoundUp] /r/" + sub + " - (" + titleDate1 + " - " + titleDate2 + ")"
    else:
        postTitle = "Daily RoundUp for /r/" + sub + " - (" + titleDate1 + ")"

    returnArray = [output, postTitle]
    return returnArray

# Check titles of posts for posts that should be added
def scanSubTitles():
    for subr in sSubreddit:
        log.info('Searching ' + subr + ' titles')
        subreddit = r.get_subreddit(subr)
        submissions = subreddit.get_new(limit=100)

    # http://www.reddit.com/r/redditdev/comments/2f11b6/is_there_a_way_to_get_a_users_comments_based_on_a/
        for post in submissions:
            sTitle = post.title
            if any(skey.lower() in sTitle.lower() for skey in sTitlestring):
                try:
                    sAuthor = post.author.name
                except AttributeError:
                    sAuthor = '[DELETED]'
                sTime = int(post.created_utc)

                thing_id = post.id
                cur.execute('SELECT * FROM saveposts WHERE ID=?', [thing_id])
                if not cur.fetchone(): #If not in database
                    try:
                        sqlTitle = sTitle.split("]")
                        sqlTitle[0] += "]"
                        sqlMainTitle = sqlTitle[1].lstrip(' ')
                    except IndexError:
                        sqlMainTitle = "Error in title name"
                    sLink = post.permalink

                    returnvar = getCType(sTitle)
                    sType = returnvar[0]
                    sTitle = returnvar[1]

#Thing ID | Subreddit | Type | Title | UTC_CREATED (h-5=CST) | Author
                    params = (thing_id, subr, sType, sTitle, sTime, sAuthor)
                    cur.execute('INSERT INTO saveposts VALUES(?,?,?,?,?,?)', params)
                    log.warning('Adding link from ' + sAuthor + ' to saveposts: ' + sLink)
                    # post.add_comment(SUBREPLYSTRING + FOOTER + sLink + ') ^' + VERSION)
                    # Automatic comments were getting annoying. Commented out for now

                    # if RECIPIENT != sAuthor:
                    #     r.send_message(RECIPIENT, MSTITLE, '**RoundUp_bot**\n\n' + sAuthor +
                    #                    ' has used one of your keywords in their submission title.\n\n[' +
                    #                    post.title + '](' + sLink + ')')
                    sql.commit()
            sql.commit()
    log.info('Done with submission titles')

# Check comments in posts for posts that should be added
def scanSubComments():
    for sub in sSubreddit:
        log.info('Searching ' + sub + '\'s comments')
        # http://www.reddit.com/r/jailbreak/comments/?limit=500
        allComments = r.get_content("http://www.reddit.com/r/%s/comments/.json?limit=%s" % (sub, 500))

        try:
            for comment in allComments:
                cBody = comment.body
                cBody_low = cBody.lower()

                if any(key.lower() in cBody_low for key in cBotmention):
                    cAuthor = str(comment.author)
                    cTitle = comment.link_title

                    print cTitle

                    thing_id=str(comment.id)
                    cur.execute('SELECT * FROM saveposts WHERE ID=?', [thing_id])

                    if not cur.fetchone(): # If submitted thing_id dosen't exist in sql table

                        print "Not in Database"

                        if cAuthor == sUsername:
                            print "Won't reply to myself"
                            continue

                        if '[roundup' in cTitle.lower():
                            comment.reply(cCommentreply[1])
                            params = (thing_id, "", "", "", "", "")
                            cur.execute('INSERT INTO saveposts VALUES(?,?,?,?,?,?)', params)

                        elif "[request" in cTitle.lower() or "(request" in cTitle.lower():
                            comment.reply(cCommentreply[2])
                            params = (thing_id, "", "", "", "", "")
                            cur.execute('INSERT INTO saveposts VALUES(?,?,?,?,?,?)', params)

                        elif "/u/roundupbot" in cBody_low:
                            comment.reply(cCommentreply[4])
                            params = (thing_id, "", "", "", "", "")
                            cur.execute('INSERT INTO saveposts VALUES(?,?,?,?,?,?)', params)

                        elif 'add' in cBody_low and 'roundup' in cBody_low:

                            print "Keywords 'and' and 'roundup' found in post"

                            returnvar = getCType(cTitle)
                            cType = returnvar[0]
                            cTitle = returnvar[1]

                            cTitle.lstrip(' ')
                            cTitle.rstrip(' ')
                            cTime = comment.created_utc
                            cLink = comment.link_url

                            params = (thing_id, sub, cType, cTitle, str(cTime), cAuthor)
                            print cType
                            cur.execute('INSERT INTO saveposts VALUES(?,?,?,?,?,?)', params)
                            # Author
                            if cAuthor == sAdmin:
                                log.debug("Username = Admin")
                                rand = random.randint(0, len(cObedient)-1)
                                comment.reply(cObedient[rand] + cFooter + cLink + ")")

                                print "Commenting to '" + cAuthor + "' after adding post"

                            else:
                                try:
                                    comment.reply(cCommentreply[0] + cFooter + cLink + ")")

                                    print "Commenting to '" + cAuthor + "' after adding post"

                                except Exception: #Comment has been deleted
                                    print comment.link_url
                                    continue
                    else:
                        replied = False
                        for comReply in comment.replies:
                            if str(comReply.author) == sUsername:
                                print "Already Replied. Not going to reply again."
                                replied = True
                                break
                        if not replied:
                            print "Replying for the first time?"
                            #comment.reply(cCommentreply[3] + cFooter + comment.link_url + ")")
                    sql.commit()
        except Exception: #Called when there are no new comments
            print(traceback.format_exc())

# Get Content type. Try to get [tag]. Otherwise, use [Thread]
def getCType(cTitle):
    bCount = cTitle.count(']')
    print "] count: " + str(bCount)
    if bCount == 0:
        cType = "[Thread]"
    elif bCount == 1:
        sqlTitle = cTitle.split("]")
        cType = sqlTitle[0] + "]"
        cTitle = cTitle.replace(cType, "")
    elif bCount > 1:
        depth = cTitle.find(']')+1
        cType = cTitle[:depth]
        cTitle = cTitle.replace(cType, "")
    cTitle.strip(" ")
    returnvar = [cType, cTitle]
    return returnvar

#Execute main functions
try:
    scanSubComments()
except Exception:
    print(traceback.format_exc())
