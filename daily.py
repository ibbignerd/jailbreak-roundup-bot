import reddit as re

import praw
import datetime
import logging
import sqlite3
import time
import traceback
import random
from dateutil import parser


# Bot reddit username
sUsername = ''
# Bot reddit password
sPassword = ''
# Build version
sVersion = ''
# reddit user agent
sUseragent = ''
# Subreddits to search separated by a '+'
sSubreddit = ''
# reddit username of the admin of the bot
sAdmin = ''


try:
    import bot
    # This is a file in my python library which contains my
    # Bot's username and password.
    # I can push code without showing credentials
    sUsername = bot.uG
    sPassword = bot.pG
    sUseragent = bot.aG
    sVersion = bot.vG
    sSubreddit = bot.sG
    sAdmin = bot.dG
except ImportError:
    pass

# INFO: Returns: [08-05 12:07:53] [LEVEL ] message in console
FORMAT = '[%(asctime)-13s] [%(levelname)-6s] %(message)s'
DATE_FORMAT = '%m-%d %H:%M:%S'
formatter = logging.Formatter(fmt=FORMAT, datefmt=DATE_FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
log = logging.getLogger(__name__)
# INFO: Set logging level: INFO, DEBUG, WARNING, ERROR
log.setLevel(logging.DEBUG)
log.addHandler(handler)

# INFO: Connect to sql.db file (create if doesn't exist)
sql = sqlite3.connect('sql.db')
log.info('Loaded SQL Database')
# INFO: Create cursor to carry out SQL arguments
cur = sql.cursor()


# Log into reddit account
r = praw.Reddit(sUseragent + sVersion)
r.login(sUsername, sPassword)
log.info('Logged into reddit')

bodyFooter = "\n\n*You are recieving this message because you signed up for Daily RoundUp messages. If you would like, you may [unsubscribe](http://www.reddit.com/message/compose/?to=RoundUp_bot&subject=subscribe&message=remove). If you see any major issues, please [send me a message](http://www.reddit.com/message/compost/?to=ibbignerd&subject=Daily%20Roundup%20Question)."

subreddits = sSubreddit.split('+') # Used for multiple subreddits
for subr in subreddits:
    returnArray = re.generateRoundUp(subr, "daily")
    subject = returnArray[1]
    body = returnArray[0] = bodyFooter

    for line in cur.execute('SELECT * FROM subscriptions WHERE SUB = ?', subr):
        r.send_message(line[0], subject, body)
